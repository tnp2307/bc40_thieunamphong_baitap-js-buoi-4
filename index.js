function tinh1() {
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var num3 = document.getElementById("num3").value * 1;
  if (num1 < num2 && num2 < num3) {
    document.getElementById("result-1").innerHTML = `${num1},${num2},${num3}`;
  } else if (num1 < num3 && num3 < num2) {
    document.getElementById("result-1").innerHTML = `${num1},${num3},${num2}`;
  } else if (num2 < num1 && num1 < num3) {
    document.getElementById("result-1").innerHTML = `${num2},${num1},${num3}`;
  } else if (num2 < num3 && num3 < num1) {
    document.getElementById("result-1").innerHTML = `${num2},${num3},${num1}`;
  } else if (num3 < num1 && num1 < num2) {
    document.getElementById("result-1").innerHTML = `${num3},${num1},${num2}`;
  } else if (num3 < num2 && num2 < num1) {
    document.getElementById("result-1").innerHTML = `${num3},${num2},${num1}`;
  }
}
function tinh2() {
  var thanhVienEl = document.getElementById("thanh-vien").value * 1;
  //   if (thanhVienEl == 2) {
  //     document.getElementById("result-2").innerHTML = "Bố !";
  //   } else if (thanhVienEl == 3) {
  //     document.getElementById("result-2").innerHTML = "Mẹ !";
  //   } else if (thanhVienEl == 4) {
  //     document.getElementById("result-2").innerHTML = "Anh trai !";
  //   } else if (thanhVienEl == 5) {
  //     document.getElementById("result-2").innerHTML = "Em gái !";
  //   }
  switch (thanhVienEl) {
    case 2:
      document.getElementById("result-2").innerHTML = "Bố !";
      break;
    case 3:
      document.getElementById("result-2").innerHTML = "Mẹ !";
      break;
    case 4:
      document.getElementById("result-2").innerHTML = "Anh trai !";
      break;
    case 5:
      document.getElementById("result-2").innerHTML = "Em gái !";
      break;
  }
}
function tinh3() {
  var num11 = (document.getElementById("num11").value * 1) % 2;

  var num21 = (document.getElementById("num21").value * 1) % 2;
  var num31 = (document.getElementById("num31").value * 1) % 2;
  var count = 0;
  if (num11 == 0) {
    count++;
  }
  if (num21 == 0) {
    count++;
  }

  if (num31 == 0) {
    count++;
  }

  document.getElementById("result-3").innerHTML = `Có ${count} số chẵn và ${
    3 - count
  } số lẽ`;
}
function tinh4() {
  var canhA = document.getElementById("txt-a").value * 1;
  var canhB = document.getElementById("txt-b").value * 1;
  var canhC = document.getElementById("txt-c").value * 1;
  if (canhA + canhB > canhC && canhC + canhB > canhA && canhA + canhC > canhB) {
    if (canhA == canhB && canhA == canhC) {
      document.getElementById("result-4").innerHTML = `Đều`;
    } else if (canhA == canhB || canhA == canhC || canhB == canhC) {
      document.getElementById("result-4").innerHTML = `Cân`;
    } else if (
      canhA * canhA == canhB * canhB + canhC * canhC ||
      canhB * canhB == canhA * canhA + canhC * canhC ||
      canhC * canhC == canhB * canhB + canhA * canhA
    ) {
      document.getElementById("result-4").innerHTML = `Vuông`;
    }
  }
}
